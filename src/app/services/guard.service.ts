import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { DatabaseghkService } from './databaseghk.service';

@Injectable({
  providedIn: 'root'
})
export class GuardService implements CanActivate {
  
  constructor(private dbService:DatabaseghkService, private route:Router) {}

  canActivate(route:ActivatedRouteSnapshot, state: RouterStateSnapshot):Observable<boolean> | Promise<boolean> | boolean {
    return new Promise(
      (resolve, reject)=>{
        if (this.dbService.getStorage() != null) {
          resolve(true);
        }else {
          this.route.navigate(['/menu','confidential']);
          reject(false);
        }
    });
  }

}
