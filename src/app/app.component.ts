import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

declare var window:any;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();

  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleLightContent();
      this.splashScreen.hide();
      
      // let status bar overlay webview
      this.statusBar.overlaysWebView(false);

      // set status bar to color you want define
      this.statusBar.backgroundColorByHexString('#d4ad31');
      // if (this.platform.is('cordova') && this.platform.is('ios')) {
      //   this.showAppTrackingTransparency();
      // } else {
      //   // Show custom message
      // }
    });
  }

  // private showAppTrackingTransparency() {
  //   const idfaPlugin = window.cordova.plugins.idfa;
  //   idfaPlugin.getInfo().then((info) => {
  //     if (!info.trackingLimited) {
  //       return info.idfa || info.aaid;
  //     } else if (
  //       info.trackingPermission ===
  //       idfaPlugin.TRACKING_PERMISSION_NOT_DETERMINED
  //     ) {
  //       return idfaPlugin.requestPermission().then((result) => {
  //         if (result === idfaPlugin.TRACKING_PERMISSION_AUTHORIZED) {
  
  //           // Start your tracking plugin here!
  
  //           return idfaPlugin.getInfo().then((info) => {
  //             return info.idfa || info.aaid;
  //           });
  //         }
  //       });
  //     }
  //   });
  // }

  
}
