import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { LoadingController, Platform } from '@ionic/angular';
import { DatabaseghkService } from 'src/app/services/databaseghk.service';

@Component({
  selector: 'app-room',
  templateUrl: './room.page.html',
  styleUrls: ['./room.page.scss'],
})
export class RoomPage implements OnInit, OnDestroy {

  sideContent;

  user:any[];

  counterSubscription: Subscription;

  contentSubscription: Subscription;
  backButtonSub: Subscription;

  slideopts = {
    initialSlide : 0,
    speed: 400,
    autoplay: true
  };

  constructor(private platform:Platform, private dbService:DatabaseghkService, private load:LoadingController) { }

  ngOnInit() {
    this.contentSubscription = this.dbService.contentSub.subscribe(
      (sideContent:any[])=>{
        this.sideContent = sideContent;
      });
    this.dbService.emitSideContent();

    this.counterSubscription = this.dbService.userSub.subscribe(
      (user:any[])=>{
        this.user = user;
      });
    this.dbService.emitUsersSub();
  }

  findIndexToUpdate(obj) {return obj.title === this;}

  onShare(content) {
    const message = content.title +' au GRAND HOTEL DE KINTELE';
    const image = "https://cick-grandhotelkintele.com/wp-content/uploads/2021/03/Dimanche-Na-Biso-960x600-1.jpg";
    const url = "https://cick-grandhotelkintele.com";
    this.dbService.shareViaWhatsapp(message, image, url);
  }

  ngOnDestroy() {
    this.contentSubscription.unsubscribe();
  }

  ionViewDidEnter() {
    this.backButtonSub = this.platform.backButton
    .subscribeWithPriority(5, () => this.dbService.backButton());
  }
  
  ionViewWillLeave() {this.backButtonSub.unsubscribe();}


}
