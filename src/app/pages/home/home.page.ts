import { Component, OnInit, OnDestroy } from '@angular/core';
import {Platform, LoadingController } from '@ionic/angular';
import { MenuController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { DatabaseghkService } from 'src/app/services/databaseghk.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit, OnDestroy {

  pet:String;
  open:Boolean = false;

  type = null;

  UUID;

  isLoad = true;

  products: any[] = [];
  users: any[] = [];
  table: any[];

  slideopts = {
    initialSlide : 0,
    speed: 400,
    autoplay: true
  };

  productSubsc: Subscription;
  userSubsc: Subscription;
  counterSubsc: Subscription;
  uucontentSubsc: Subscription;
  userAuthSubsc: Subscription;
  backButtonSub:Subscription;

  uucase;
  
  constructor(public platform:Platform, private load:LoadingController, private router: Router, private menuCtrl: MenuController, private dbService:DatabaseghkService) {}

  ngOnInit() {
    
    this.userAuthSubsc = this.dbService.userAuthSub.subscribe(
      (dataAuth)=>{
        if(dataAuth != null){
          this.UUID = this.dbService.getStorage();
          this.init();
        }
    });
    this.dbService.emitUserAuth();
  }

  

  async init() {
    const loading = await this.load.create({
      cssClass: 'my-class-moon',
      spinner: 'crescent',
      message: 'Patientez s\'il vous plait',
    });
    await loading.present();
    this.dbService.loadDatabase()
    .then((res)=>{
      if (res != null) {
        loading.dismiss();
        this.getProducts();
      } else {
        this.isLoad = false;
      }
    })
    .catch((err)=>{
      this.isLoad = false;
      loading.dismiss();
      console.log(err);
    });
  }

  async getProducts() {
    const loading = await this.load.create({
      cssClass: 'my-class-moon',
      spinner: 'crescent',
      message: 'Patientez s\'il vous plait',
    });
    await loading.present();
    this.productSubsc = this.dbService.productSub.subscribe(
      (products)=>{
        if (products) {
          this.products = products;
          this.getUUser();
          loading.dismiss();
        } else {this.isLoad = false; loading.dismiss();}
    });
    this.dbService.emitProductsSub();
  }

  async getUUser() {
    const loading = await this.load.create({
      cssClass: 'my-class-moon',
      spinner: 'crescent',
      message: 'Patientez s\'il vous plait',
    });
    await loading.present();
    this.userSubsc = this.dbService.userSub.subscribe(
      (users)=>{
        if (users) {
          this.users = users;
          loading.dismiss();
          this.dbService.checkUUI(this.UUID);
          this.getUserContent();
        } else {this.isLoad = false; loading.dismiss();}
    });
    this.dbService.emitUsersSub();
  }

  async getUserContent() {
    const loading = await this.load.create({
      cssClass: 'my-class-moon',
      spinner: 'crescent',
      message: 'Patientez s\'il vous plait',
    });
    await loading.present();
    this.uucontentSubsc = this.dbService.uucaseSub.subscribe(
      (uucase)=>{
        if (uucase) {
          this.uucase = uucase;
          loading.dismiss();
        } else {this.isLoad = false; loading.dismiss();}
    });
    this.dbService.emitUUCASE();
    this.pet = "Accueil";
      setTimeout(() => {
        this.isLoad = true;
        loading.dismiss();
      }, 2000);
  }

  fullLoad(event) {
    const file = event.target.src
    console.log("Full load", event.target.src);
  }


  openMenu(content, index){
    this.dbService.tracker[0] = index;
    if(content.url != ""){
      this.menuCtrl.enable(false);
      this.dbService.onGetSideContent(content);
        this.router.navigate([content.url]);
    }else{
      this.menuCtrl.enable(true);
      this.dbService.onGetSideMenu(content);
      this.menuCtrl.toggle();
    }
    
  }

  async doRefresh(event) {
    const loading = await this.load.create({
      cssClass: 'my-class-moon',
      spinner: 'crescent',
      message: 'Patientez s\'il vous plait',
    });
    await loading.present();
    this.dbService.loadDatabase()
    .then((res)=>{
      if (res != null) {
        loading.dismiss();
        setTimeout(() => {
          event.target.complete();
        }, 3000);
        this.getProducts();
      } else {
        this.isLoad = false;
      }
    })
    .catch((err)=>{
      this.isLoad = false;
      loading.dismiss();
      setTimeout(() => {
        event.target.complete();
      }, 3000);
      console.log(err);
    });
  }

  ngOnDestroy() {
    this.productSubsc.unsubscribe();
    this.userSubsc.unsubscribe();
    this.uucontentSubsc.unsubscribe();
    this.userAuthSubsc.unsubscribe();
  }

  ionViewDidEnter() {
    this.backButtonSub = this.platform.backButton
    .subscribeWithPriority(10, () => this.dbService.backButtonExit());
  }
  
  ionViewWillLeave() {this.backButtonSub.unsubscribe();}

  updateButton(){
    //this.dbService.UpdateproductsDATA();
  }

}
