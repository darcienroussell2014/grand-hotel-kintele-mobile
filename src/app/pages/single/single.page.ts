import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { DatePipe } from '@angular/common';
import { DatabaseghkService } from 'src/app/services/databaseghk.service';
import { Router } from '@angular/router';
import { LoadingController, Platform } from '@ionic/angular';

@Component({
  selector: 'app-single',
  templateUrl: './single.page.html',
  styleUrls: ['./single.page.scss'],
})
export class SinglePage implements OnInit, OnDestroy {

  startTime = '';
  startDate = '';

  counter:number;
  
  user:any[];

  counterSubsc: Subscription;
  uucontentSubsc: Subscription;
  bookingSubsc:Subscription;
  ordersSubsc:Subscription;

  orders = [];
  bookings = [];
  uucase;

  detailContent: any;

  detailSubscription: Subscription;
  backButtonSub:Subscription;

  QTE:number = 1;
  
  constructor(private platform:Platform, private router:Router, private load:LoadingController, private datePipe:DatePipe, private dbService:DatabaseghkService) { }

  ngOnInit() {
    this.detailSubscription = this.dbService.detailSub.subscribe(
      (detailContent:any[])=>{
        this.detailContent = detailContent;
      });
    this.dbService.emitDetailContent();

    this.bookingSubsc = this.dbService.bookingSub.subscribe(
      (bookings)=>{
        this.bookings = bookings?bookings:[];
    });
    this.dbService.emitBookings();

    this.ordersSubsc =  this.dbService.orderSub.subscribe(
      (orders)=>{
        this.orders = orders?orders:[];
    });
    this.dbService.emitOrders();

    this.uucontentSubsc = this.dbService.uucaseSub.subscribe(
      (uucase)=>{
        this.uucase = uucase;
    });
    this.dbService.emitUUCASE();

    this.counter = this.dbService.emitCounter() > 0 ? this.dbService.emitCounter() : 0;
    
  }

  onAddQte() {
    if(this.QTE <10){
      this.QTE++;
    }
  }

  onRemQte() {
    if(this.QTE > 1){
      this.QTE--;
    }
  }

  async onGetCommande() {
    const elem = {
      id: this.orders.length > 0 ?this.orders[this.orders.length-1].id+1:1,
      title: this.detailContent.title,
      price: this.detailContent.prix,
      img: this.detailContent.img,
      quantity: this.QTE,
      address: this.uucase.chambre[0].title+' '+this.uucase.chambre[0].number,
      table: this.dbService.selectedTable?this.dbService.selectedTable:this.uucase.chambre[0].title+'-'+this.uucase.chambre[0].number,
      date: {show:this.datePipe.transform(new Date(), 'dd/MM/yyyy-HH:mm'),read:Date.now()},
      status: {finish:false, abolish:false},
      user: this.uucase.id
    };

    this.orders.push(elem);
    this.dbService.openFullAlert("souhaitez-vous confirmer cette action ?", async ()=>{
      const loading = await this.load.create({
        cssClass: 'my-class-moon',
        spinner: 'crescent',
        message: 'Patientez s\'il vous plait',
      });
      await loading.present();

      this.dbService.execOrder(this.orders)
        .then(()=>{
          loading.dismiss();
          this.dbService.openToast("commende transmise avec succes !");
          this.router.navigate(['menu','restaurant']);
          
        })
        .catch((error)=>{ 
          loading.dismiss();
          console.log(error);
          this.dbService.openToast('Erreur inconnu survenu, réessayez plus tard!'); });
    });
  }

  async onGetReservation() {
    
    const elem = {
      id: this.bookings.length>0?this.bookings[this.bookings.length-1].id+1:1,
      title: this.detailContent.title,
      price: this.detailContent.prix,
      img: this.detailContent.img,
      quantity: this.QTE,
      address: this.uucase.chambre[0].title+' '+this.uucase.chambre[0].number,
      table: this.dbService.selectedTable?this.dbService.selectedTable:this.uucase.chambre[0].title+'-'+this.uucase.chambre[0].number,
      contact:this.uucase.phone,
      startDate: this.datePipe.transform(this.startDate, 'dd/MM/yyyy'),
      startTime: this.datePipe.transform(this.startTime, 'HH:mm'),
      date: {show:this.datePipe.transform(new Date(), 'dd/MM/yyyy-HH:mm'),read:Date.now()},
      status: {finish:false, abolish:false},
      user:this.uucase.id
    };

  this.bookings.push(elem);
    this.dbService.openFullAlert("souhaitez-vous confirmer cette action ?", async ()=>{
      const loading = await this.load.create({
        cssClass: 'my-class-moon',
        spinner: 'crescent',
        message: 'Patientez s\'il vous plait',
      });
      await loading.present();

      this.dbService.execBooking(this.bookings)
        .then(()=>{
          loading.dismiss();
          this.dbService.openToast("Réservation transmise avec succes !");
          this.router.navigate(['/menu','restaurant']);
          
        })
        .catch((error)=>{ 
          loading.dismiss();
          this.dbService.openToast('Erreur inconnu survenu, réessayez plus tard!'); });
    });
  }

  onShare(content) {
    const message = content.title +' au GRAND HOTEL DE KINTELE';
    const image = "https://cick-grandhotelkintele.com/wp-content/uploads/2021/03/Dimanche-Na-Biso-960x600-1.jpg";
    const url = "https://cick-grandhotelkintele.com";
    this.dbService.shareViaWhatsapp(message, image, url);
  }

  ngOnDestroy() {
    this.detailSubscription.unsubscribe();
    this.uucontentSubsc.unsubscribe();
    this.bookingSubsc.unsubscribe();
    this.ordersSubsc.unsubscribe();
    
  }

  ionViewDidEnter() {
    this.backButtonSub = this.platform.backButton
    .subscribeWithPriority(5, () => this.dbService.backButton());
  }
  
  ionViewWillLeave() {this.backButtonSub.unsubscribe();}

}
