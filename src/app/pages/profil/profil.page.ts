import { Component, OnInit, OnDestroy } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { DatabaseghkService } from 'src/app/services/databaseghk.service';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.page.html',
  styleUrls: ['./profil.page.scss'],
})
export class ProfilPage implements OnInit, OnDestroy {

  sideContent;
  user: any[];

  counter:number;
  counterSubsc:Subscription;
  uucontentSubsc:Subscription;
  bookingSubsc:Subscription;
  ordersSubsc:Subscription;
  contentSubsc:Subscription;
  userSubsc:Subscription;
  backButtonSub:Subscription;
  
  users = [];
  orders = [];
  bookings = [];
  uucase;

  userHost = {name:null, phone:null, email: null};

  error = {
    name:{ status: false, value: "caractère non permis !"},
    phone:{ status: false, value: "téléphone incorrect !"},
    email:{ status: false, value: "e-mail incorrect !"}
  };

  isvisible = false;

  constructor(private platform:Platform, private dbService:DatabaseghkService) { }

  ngOnInit() {
    this.contentSubsc = this.dbService.contentSub.subscribe(
      (sideContent:any[])=>{
        this.sideContent = sideContent;
      });
    this.dbService.emitSideContent();

    this.userSubsc = this.dbService.userSub.subscribe(
      (users)=>{
        this.users = users?users:[];
    });
    this.dbService.emitUsersSub();

    this.bookingSubsc = this.dbService.bookingSub.subscribe(
      (bookings)=>{
        this.bookings = bookings?bookings:[];
    });
    this.dbService.emitBookings();

    this.ordersSubsc =  this.dbService.orderSub.subscribe(
      (orders)=>{
        this.orders = orders?orders:[];
    });
    this.dbService.emitOrders();

    this.uucontentSubsc = this.dbService.uucaseSub.subscribe(
      (uucase)=>{
        this.uucase = uucase;
    });
    this.dbService.emitUUCASE();

    this.counter = this.dbService.emitCounter() > 0 ? this.dbService.emitCounter() : 0;

  }

  bookingFilter() {
    let data = [];
    for (let index0 = 0; index0 < this.bookings.length; index0++) {
      const element = this.bookings[index0];
      if (element.user == this.uucase.id) {
        data.push(element);
      }
    }
    return data;
  }

  orderFilter() {
    let data = [];
    for (let index0 = 0; index0 < this.orders.length; index0++) {
      const element = this.orders[index0];
      if (element.user == this.uucase.id) {
        data.push(element);
      }
    }
    return data;
  }

  checkVisibility(){
    this.isvisible = !this.isvisible;
  }

  handdle1(event) {
    if ((event.target.value.match(/^[A-Za-z_-\s]+$/)) || event.target.value === "") {
      this.error.name.status = false;
      this.userHost.name = event.target.value;
    } else {this.error.name.status = true;}
  }

  handdle2(event) {
    if ((event.target.value.match(/^[0-9+]+$/)) || event.target.value === "") {
      this.error.phone.status = false;
      this.userHost.phone = event.target.value;
    } else {this.error.phone.status = true;}
  }


  handdle3(event) {
    if (event.target.value.match(/^([\w-\.]+)@((?:[\w]+\.)+)([a-zA-Z]{2,4})/i) || event.target.value === "" ) {
      this.error.email.status = false;
      this.userHost.email = event.target.value;
    } else {this.error.email.status = true;}
  }

  saveChange() {
    const userForm = {
      id: this.uucase.id,
      name: this.userHost.name?this.userHost.name:this.uucase.name,
      email: this.userHost.email?this.userHost.email:this.uucase.email,
      phone: this.uucase.phone,
      pays: this.uucase.pays,
      created: this.uucase.created,
      flag: this.uucase.flag,
      chambre: [{title:this.uucase.chambre[0].title, number: this.uucase.chambre[0].number}],
      credit: [{start:this.uucase.credit[0].start, end:this.uucase.credit[0].end}]
    };
    this.findIndexToUpdateBack(userForm);
    let updateItem = this.users.find(this.findIndexToUpdateBack, userForm.id);
    let index = this.users.indexOf(updateItem);
  
    if(index > -1){
      this.users[index] = userForm;
      this.dbService.openFullAlert("souhaitez-vous enregistrer ces modifications ?", ()=>{
        this.dbService.openLoader(true);
        this.dbService.signUp(this.users)
        .then(()=>{
          setTimeout(() => {
            this.dbService.openLoader(false);
            this.checkVisibility();
          }, 2000);
          this.dbService.openToast("Modifications Enregistrées avec succès !");
        })
        .catch(()=>{
          setTimeout(() => {
            this.dbService.openLoader(false);
            this.checkVisibility();
          }, 2000);
          this.dbService.openToast("Echèc d\'enregistrement, réessayez plus tard !");
        });
      })
    }
  }

  findIndexToUpdateBack(obj) {return obj.id === this;}

  ngOnDestroy(){
    this.uucontentSubsc.unsubscribe();
    this.bookingSubsc.unsubscribe();
    this.ordersSubsc.unsubscribe();
    this.contentSubsc.unsubscribe();
  }

  ionViewDidEnter() {
    this.backButtonSub = this.platform.backButton
    .subscribeWithPriority(5, () => this.dbService.backButton());
  }
  
  ionViewWillLeave() {this.backButtonSub.unsubscribe();}

}
