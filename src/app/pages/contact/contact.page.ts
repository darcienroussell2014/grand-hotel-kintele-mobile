import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { DatabaseghkService } from 'src/app/services/databaseghk.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {

  sideContent;

  contentSubsc: Subscription;
  backButtonSub:Subscription;

  form = {
    nom: '',
    mail: '',
    message: ''
  };

  constructor(private platform:Platform, private dbService:DatabaseghkService) { }

  ngOnInit() {}

  sendmail() {
    const Subject = this.form.nom;
    const body = this.form.mail + ' \n' + this.form.message;
    this.dbService.shareViaMail(Subject, body)
    .then(()=>{
      this.dbService.openToast('Message transmis avec succès !');
    }).catch(()=>{
        this.dbService.openToast('Echèc, erreur inconnu, réessayer plus tard!');
    });
  }

  ionViewDidEnter() {
    this.backButtonSub = this.platform.backButton
    .subscribeWithPriority(5, () => this.dbService.backButton());
  }
  
  ionViewWillLeave() {this.backButtonSub.unsubscribe();}


}
