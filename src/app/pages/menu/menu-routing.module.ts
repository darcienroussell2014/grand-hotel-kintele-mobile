import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GuardService } from '../../services/guard.service';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: 'menu',
    component: MenuPage,
    children : [
      {
        path: 'home',
        canActivate: [GuardService],
        loadChildren: () => import('../home/home.module').then( m => m.HomePageModule)
      },
      {
        path: 'restaurant',
        canActivate: [GuardService],
        loadChildren: () => import('../restaurant/restaurant.module').then( m => m.RestaurantPageModule)
      },
      {
        path: 'contact',
        canActivate: [GuardService],
        loadChildren: () => import('../contact/contact.module').then( m => m.ContactPageModule)
      },
      {
        path: 'profil',
        canActivate: [GuardService],
        loadChildren: () => import('../profil/profil.module').then( m => m.ProfilPageModule)
      },
      {
        path: 'room',
        canActivate: [GuardService],
        loadChildren: () => import('../room/room.module').then( m => m.RoomPageModule)
      },
      {
        path: 'center',
        canActivate: [GuardService],
        loadChildren: () => import('../center/center.module').then( m => m.CenterPageModule)
      },
      {
        path: 'spa',
        canActivate: [GuardService],
        loadChildren: () => import('../spa/spa.module').then( m => m.SpaPageModule)
      },
      {
        path: 'single',
        canActivate: [GuardService],
        loadChildren: () => import('../single/single.module').then( m => m.SinglePageModule)
      },
      {
        path: 'signin',
        loadChildren: () => import('../signin/signin.module').then( m => m.SigninPageModule)
      },
      {
        path: 'signup',
        loadChildren: () => import('../signup/signup.module').then( m => m.SignupPageModule)
      },
      {
        path: 'confidential',
        loadChildren: () => import('../confidential/confidential.module').then( m => m.ConfidentialPageModule)
      }
    ]
  },
  { 
    path:'', 
    redirectTo: '/menu/home'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuPageRoutingModule {}
