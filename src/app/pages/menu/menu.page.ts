import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { DatabaseghkService } from 'src/app/services/databaseghk.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit, OnDestroy {

  sideMenu;

  menuSubsc: Subscription;

  constructor(private router: Router,  private dbService:DatabaseghkService) { }

  ngOnInit() {
    
    this.menuSubsc = this.dbService.menuSub.subscribe(
      (sideMenu)=>{
        this.sideMenu = sideMenu;
      });
    this.dbService.emitSideMenu();
  }

  onRedirectTo(content, index){
    this.dbService.tracker[1] = index;
    this.dbService.onGetSideContent(content);
    this.router.navigate([content.url]);
  }

  ngOnDestroy() {
    this.menuSubsc.unsubscribe();
  }

}
