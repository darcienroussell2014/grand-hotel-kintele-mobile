import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ConfidentialPage } from './confidential.page';

describe('ConfidentialPage', () => {
  let component: ConfidentialPage;
  let fixture: ComponentFixture<ConfidentialPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfidentialPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ConfidentialPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
