import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-confidential',
  templateUrl: './confidential.page.html',
  styleUrls: ['./confidential.page.scss'],
})
export class ConfidentialPage implements OnInit {

  contentView = false;

  constructor(private router:Router) { }

  ngOnInit() {
  }

  changeState(){
    this.contentView = !this.contentView;
  }

  routePage(index){
    this.router.navigate(['menu',index]);
  }

}
