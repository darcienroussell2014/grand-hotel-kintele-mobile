import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConfidentialPageRoutingModule } from './confidential-routing.module';

import { ConfidentialPage } from './confidential.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConfidentialPageRoutingModule
  ],
  declarations: [ConfidentialPage]
})
export class ConfidentialPageModule {}
