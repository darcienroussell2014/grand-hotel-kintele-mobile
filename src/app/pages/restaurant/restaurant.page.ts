import { Component, OnInit, OnDestroy } from '@angular/core';
import { interval, Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { DatabaseghkService } from 'src/app/services/databaseghk.service';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant.page.html',
  styleUrls: ['./restaurant.page.scss'],
})
export class RestaurantPage implements OnInit, OnDestroy {

  sideContent;

  selectedTab = "1";
  searchTerm = '';
  contentList = [];
  table = [];
  selectedTable = null;
  seletedVisible = false;
  TabColor = false;

  counter = 0;

  contentSubscription: Subscription;
  TabcolorSubscription: Subscription;
  backButtonSub: Subscription;

  constructor(private platform:Platform, private router: Router, private dbService:DatabaseghkService) { }

  ngOnInit() {
    this.contentSubscription = this.dbService.contentSub.subscribe(
      (sideContent:any[])=>{
        this.sideContent = sideContent;
      });
    this.dbService.emitSideContent();

    this.counter = this.dbService.emitCounter() > 0 ? this.dbService.emitCounter() : 0;

    this.table = this.dbService.table;
    this.selectedTable = this.dbService.selectedTable;
    this.animationTab();
  }

  handle1(event) {
    this.seletedVisible = !this.seletedVisible;
    this.dbService.selectedTable = event.detail.value;
    this.selectedTable = event.detail.value;
  }

  getRoom(){
    this.seletedVisible = !this.seletedVisible;
  }

  closePage() {
    this.seletedVisible = !this.seletedVisible;
    this.dbService.selectedTable = null;
    this.router.navigate(['menu','home']);
  }

  animationTab() {
    const states = interval(500);
    this.TabcolorSubscription = states.subscribe(()=>{
        this.TabColor = ! this.TabColor;
        return this.TabColor;
    })
  }

  // filterItems() {
  //   const address = this.dbService.tracker;
  //   const tbContentSEARCH = [];
  //   console.log(this.sideContent.content);
  //   this.contentList = tbContentSEARCH.filter(item => {
  //     return item.designation.toLowerCase().match(this.searchTerm.toLowerCase());
  //   });
      
  // }

  onGetSelection(index) {
    this.dbService.tracker[2] = index;
  }

  onGetcategorie(index) {
    this.dbService.tracker[3] = index;
  }

  singleGetDetail(detail, index){
    this.dbService.tracker[4] = index;
    this.dbService.onGetSingleDetail(detail);
    this.router.navigate([detail.url]);
  }

  ngOnDestroy() {
    this.contentSubscription.unsubscribe();
    this.TabcolorSubscription.unsubscribe();
  }

  ionViewDidEnter() {
    this.backButtonSub = this.platform.backButton
    .subscribeWithPriority(5, () => this.dbService.backButton());
  }
  
  ionViewWillLeave() {this.backButtonSub.unsubscribe();}


}
