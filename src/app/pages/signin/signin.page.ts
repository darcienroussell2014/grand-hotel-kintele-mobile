import { Component, OnInit, OnDestroy } from '@angular/core';
import { LoadingController, MenuController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { DatabaseghkService } from 'src/app/services/databaseghk.service';
import { Router } from '@angular/router';
declare var SMSRecieve:any;

@Component({
  selector: 'app-signin',
  templateUrl: './signin.page.html',
  styleUrls: ['./signin.page.scss'],
})
export class SigninPage implements OnInit, OnDestroy {

  countrySubsc : Subscription;

  usersSubsc : Subscription;

  userAuthSubsc:Subscription;

  country:any[];

  users;

  verificationID = {
    send: '',
    received: ''
  };

  formContent = {
    selectedCountry:{name: "Congo", dial_code: "+242", code: "CG"},
    phone: '',
    pseudo: '',
    email: ''
  };
  
  isVisible = {
    phone: false,
    button: false
  };

  userCase;

  constructor(private router:Router, private dbService:DatabaseghkService, private load:LoadingController, private menuCtrl:MenuController) { }

  ngOnInit() {

    this.isVisible = {
      phone: false,
      button: false
    };
    this.menuCtrl.enable(false);
    this.countrySubsc =  this.dbService.countrySub.subscribe(
      (country)=>{ this.country = country?country:[];});
    this.dbService.emitCountry();
    this.userAuthSubsc = this.dbService.userAuthSub.subscribe(
      (dataAuth)=>{
        if(dataAuth != null){
          this.init();
        }
    });
    this.dbService.emitUserAuth();
  }

  init() {
    this.dbService.loadDatabase()
    .then((res)=>{
      if (res != null) {
        this.usersSubsc = this.dbService.userSub.subscribe(
        (users)=>{
          this.users = users?users:[];
        });
        this.dbService.emitUsersSub();
        
      } else {
        this.dbService.openToast("Erreur de chargement..");
      }

    })
    .catch(()=>{
      this.dbService.openToast("Erreur de chargement..");
    });
    return this.users;
  }

  userControle() {
    const getUser = this.users?.length > 0 ?this.users:null;
    if (getUser != null) {

       const userForm = 
        {
          id: null,
          name: null,
          email: null,
          phone: this.formContent.selectedCountry.dial_code+this.formContent.phone,
          pays: this.formContent.selectedCountry.name,
          created: new Date(),
          flag: 'https://www.countryflags.io/'+this.formContent.selectedCountry.code+'/flat/32.png',
          chambre: [{title:'aucune chambre', number: 0}],
          credit: [{satrt:Date.now(), end:Date.now()}]
        };

        this.findIndexToUpdateBack(userForm);
        let updateItem = this.users.find(this.findIndexToUpdateBack, userForm.phone);
        let index = this.users.indexOf(updateItem);
      
        if(index > -1){
          this.userCase = this.users[index];
          this.recoveryUser();
        }else{ 
          this.dbService.openToast("ce numéro n'existe pas, réessayez !");
        } 
    }else{
      this.dbService.openToast("Erreur de chargement, réesayez plus tard");
    }
    
  }

  findIndexToUpdateBack(obj) {return obj.phone === this;}

  async recoveryUser() {
    const loading = await this.load.create({
      cssClass: 'my-class-moon',
      spinner: 'crescent',
      message: 'Patientez s\'il vous plait',
    });
    await loading.present();
      const id = this.userCase? this.userCase.id : null
    if (id != null) {
      loading.dismiss();
      this.dbService.setStorage(id);
      this.dbService.openToast("Nous somme heureux de vous revoir !");
      this.router.navigate(['/menu','home']);
      
    } else {
      loading.dismiss();
      this.dbService.openToast("Erreur de chargement, réessayez plus tard !");
    }
  }

  doRefresh(event) {

    this.isVisible = {
      phone: false,
      button: false
    };
    this.menuCtrl.enable(false);
    this.countrySubsc =  this.dbService.countrySub.subscribe(
      (country)=>{ this.country = country?country:[];});
    this.dbService.emitCountry();
    this.userAuthSubsc = this.dbService.userAuthSub.subscribe(
      (dataAuth)=>{
        if(dataAuth != null){
          this.init();
          setTimeout(() => {
            event.target.complete();
          }, 3000);
        }
    });
    this.dbService.emitUserAuth();
  }

  routePage(index){
    this.router.navigate(['menu',index]);
  }

  ngOnDestroy() {
    this.countrySubsc.unsubscribe();
    this.userAuthSubsc.unsubscribe();
  }

}
