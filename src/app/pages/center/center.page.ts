import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { DatePipe } from '@angular/common';
import { DatabaseghkService } from 'src/app/services/databaseghk.service';
import { Router } from '@angular/router';
import { LoadingController, Platform } from '@ionic/angular';

@Component({
  selector: 'app-center',
  templateUrl: './center.page.html',
  styleUrls: ['./center.page.scss'],
})
export class CenterPage implements OnInit {

  sideContent;

  user:any[];

  counterSubsc: Subscription;
  uucontentSubsc: Subscription;
  bookingSubsc:Subscription;
  ordersSubsc:Subscription;
  contentSubsc: Subscription;
  backButtonSub:Subscription;

  orders = [];
  bookings = [];
  uucase;
  counter:number;

  QTE = 1;

  startDate = "";
  startTime = "";

  selectedTab = 'Théatre';

  constructor(private platform:Platform, private router:Router, private load:LoadingController, private dbService:DatabaseghkService, private datePipe:DatePipe) { }

  ngOnInit() {
    this.contentSubsc = this.dbService.contentSub.subscribe(
      (sideContent:any[])=>{
        this.sideContent = sideContent;
        this.selectedTab = this.sideContent["content"][0].title;
      });
    this.dbService.emitSideContent();

    this.bookingSubsc = this.dbService.bookingSub.subscribe(
      (bookings)=>{
        this.bookings = bookings?bookings:[];
    });
    this.dbService.emitBookings();

    this.ordersSubsc =  this.dbService.orderSub.subscribe(
      (orders)=>{
        this.orders = orders?orders:[];
    });
    this.dbService.emitOrders();

    this.uucontentSubsc = this.dbService.uucaseSub.subscribe(
      (uucase)=>{
        this.uucase = uucase;
    });
    this.dbService.emitUUCASE();

    this.counter = this.dbService.emitCounter() > 0 ? this.dbService.emitCounter() : 0;
  }

  onShare(content) {
    const message = content.title +' au GRAND HOTEL DE KINTELE';
    const image = "https://cick-grandhotelkintele.com/wp-content/uploads/2021/03/Dimanche-Na-Biso-960x600-1.jpg";
    const url = "https://cick-grandhotelkintele.com";
    this.dbService.shareViaWhatsapp(message, image, url);
  }

  onAddQte() {
    if(this.QTE <10){
      this.QTE++;
    }
  }

  onRemQte() {
    if(this.QTE > 1){
      this.QTE--;
    }
  }

  onGetSelection(index) {
    this.dbService.tracker[2] = index;
  }

  async onGetReservation(content) {
    
    const elem = {
      id: this.bookings.length>0?this.bookings[this.bookings.length-1].id+1:1,
      title: content.title,
      price: content.content[this.dbService.tracker[2]].content[0].prix,
      configuration: content.content[this.dbService.tracker[2]].title,
      img: content.img,
      quantity: this.QTE,
      address: this.uucase.chambre[0].title+' '+this.uucase.chambre[0].number,
      contact:this.uucase.phone,
      startDate: this.datePipe.transform(this.startDate, 'dd/MM/yyyy'),
      startTime: this.datePipe.transform(this.startTime, 'HH:mm'),
      date: {show:this.datePipe.transform(new Date(), 'dd/MM/yyyy-HH:mm'),read:Date.now()},
      status: {finish:false, abolish:false},
      user:this.uucase.id
    };
    

  this.bookings.push(elem);
    this.dbService.openFullAlert("souhaitez-vous confirmer cette action ?", async ()=>{
     
      const loading = await this.load.create({
        cssClass: 'my-class-moon',
        spinner: 'crescent',
        message: 'Patientez s\'il vous plait',
      });
      await loading.present();

      this.dbService.execBooking(this.bookings)
        .then(()=>{
          setTimeout(() => {
            loading.dismiss();
            this.dbService.openToast("Réservation transmise avec succes !");
            this.router.navigate(['menu','home']);
          }, 2000);
          
        })
        .catch((error)=>{ 
          setTimeout(() => {
            loading.dismiss();
            this.dbService.openToast('Erreur inconnu survenu, réessayez plus tard!'); 
          }, 2000);
        });
    });
  }

  ngOnDestroy() {
    this.uucontentSubsc.unsubscribe();
    this.bookingSubsc.unsubscribe();
    this.ordersSubsc.unsubscribe();
    this.contentSubsc.unsubscribe();
  }

  ionViewDidEnter() {
    this.backButtonSub = this.platform.backButton
    .subscribeWithPriority(5, () => this.dbService.backButton());
  }
  
  ionViewWillLeave() {this.backButtonSub.unsubscribe();}


}
