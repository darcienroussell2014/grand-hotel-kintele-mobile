import { DatePipe } from '@angular/common';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, MenuController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { DatabaseghkService } from 'src/app/services/databaseghk.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})

export class SignupPage implements OnInit, OnDestroy {

  customAlertOptions: any = {
    header: 'Choisissez votre pays',
  };

  countrySubsc : Subscription;

  usersSubsc : Subscription;

  userAuthSubsc: Subscription;

  country:any[];

  users;

  verificationID = {
    send: '',
    received: ''
  };

  formContent = {
    selectedCountry:{name: "Congo", dial_code: "+242", code: "CG"},
    phone: '',
    pseudo: '',
    email: ''
  };
  
  isVisible = {
    phone: false,
    button: false
  };

  viewer = false;
  

  constructor(private router:Router, private datePipe:DatePipe, private load:LoadingController, private menuCtrl:MenuController, private dbService:DatabaseghkService) { }

  ngOnInit() {
    
    this.isVisible = {
      phone: false,
      button: false
    };
    this.menuCtrl.enable(false);
    this.countrySubsc =  this.dbService.countrySub.subscribe(
      (country)=>{ this.country = country?country:[];});
    this.dbService.emitCountry();
    this.userAuthSubsc = this.dbService.userAuthSub.subscribe(
      (dataAuth)=>{
        if(dataAuth != null){
          this.init();
        }
    });
    this.dbService.emitUserAuth();
    
  }

  init() {
    this.dbService.loadDatabase()
    .then((res)=>{
      if(res) {
        this.usersSubsc = this.dbService.userSub.subscribe(
          (users)=>{
            this.users = users?users:[];
          });
          this.dbService.emitUsersSub();
      }else {
        this.dbService.openToast("Erreur de chargement..");
      }
    })
    .catch(()=>{
      this.dbService.openToast("Erreur de chargement..");
    });
  }

  clearVerification() {
    this.isVisible = {
      phone: false,
      button: false
    };
    this.ngOnInit();
    this.verificationID.received = null;
  }

  userControlle() {
    const getUser = this.users?.length > 0 ?this.users:null;
    if (getUser != null) {
      this.isVisible.button = false;
      const userForm = {
        id: this.users[this.users.length-1].id+1,
        name: this.formContent.pseudo,
        email: this.formContent.email,
        phone: this.formContent.selectedCountry.dial_code+this.formContent.phone,
        pays: this.formContent.selectedCountry.name,
        created: new Date(),
        flag: 'https://www.countryflags.io/'+this.formContent.selectedCountry.code+'/flat/32.png',
        chambre: [{title:'aucune chambre', number: 0}],
        credit: [{start:Date.now(), end:Date.now()}]
      };

      this.findIndexToUpdateBack(userForm);
      let updateItem = this.users.find(this.findIndexToUpdateBack, userForm.phone);
      let index = this.users.indexOf(updateItem);
      
      if(index > -1){
        this.dbService.openToast("ce numéro existe déjà!");
      }else{
        this.createUser();
      } 
    }else{
      this.dbService.openToast("Erreur de chargement, réesayez plus tard");
    }
  }

  findIndexToUpdateBack(obj) {return obj.phone === this;}

  async createUser() {
    const loading = await this.load.create({
      cssClass: 'my-class-moon',
      spinner: 'crescent',
      message: 'Patientez s\'il vous plait',
    });
    await loading.present();

    if (this.users?.length > 0) {

      if (this.formContent.pseudo && this.formContent.pseudo != '' && this.formContent.pseudo != null) {

        if (this.formContent.phone && this.formContent.phone != '' && this.formContent.phone != null) {

          if (this.formContent.selectedCountry.name && this.formContent.selectedCountry.name != '' && this.formContent.selectedCountry.name != null) {

            const userForm = {
              id: this.users.length > 0?this.users[this.users.length-1].id+1:1,
              name: this.formContent.pseudo,
              email: this.formContent.email?this.formContent.email:'pas d\'adresse e-mail',
              phone: this.formContent.selectedCountry.dial_code+this.formContent.phone,
              pays: this.formContent.selectedCountry.name,
              created: this.datePipe.transform(new Date(),'dd/MM/yyyy'),
              flag: 'https://www.countryflags.io/'+this.formContent.selectedCountry.code+'/flat/32.png',
              chambre: [{title:'aucune chambre', number: 0}],
              credit: [{start:Date.now(), end:Date.now()}]
            };
      
            this.users.push(userForm);
            this.dbService.signUp(this.users)
            .then(()=>{
              this.dbService.setStorage(userForm.id);
              this.dbService.openToast("Merci de nous rejoindre !");
              this.router.navigate(['/menu','home']);
              loading.dismiss();
            })
            .catch(()=>{
              loading.dismiss();
              this.dbService.openToast("Erreur inconnu survenu, réessayez plus tard !");
            })
            
          }else{
            loading.dismiss();
            this.dbService.openToast("Erreur inconnu survenu, réessayez plus tard !");
          }

        }else{
          loading.dismiss();
          this.dbService.openToast("Erreur inconnu survenu, réessayez plus tard !");
        }

      }else{
        loading.dismiss();
        this.dbService.openToast("Erreur inconnu survenu, réessayez plus tard !");
      }

    }else{
      loading.dismiss();
      this.dbService.openToast("Erreur inconnu survenu, réessayez plus tard !");
    }

  }

  async skipNow() {
    const loading = await this.load.create({
      cssClass: 'my-class-moon',
      spinner: 'crescent',
      message: 'Patientez s\'il vous plait',
    });
    await loading.present();
    const getUser = this.users?.length > 0 ?this.users[0]:null;

    if (getUser != null) {
      loading.dismiss();
      this.dbService.setStorage(getUser.id);
      this.dbService.openToast("Nous somme heureux de vous revoir !");
      this.router.navigate(['/menu','home']);
    }else{
      loading.dismiss();
      this.dbService.openToast("Erreur de chargement, réesayez plus tard");
    }
  }

  viewChange() { this.viewer = !this.viewer}

  doRefresh(event) {
    this.isVisible = {
      phone: false,
      button: false
    };
    this.menuCtrl.enable(false);
    this.countrySubsc =  this.dbService.countrySub.subscribe(
      (country)=>{ this.country = country?country:[];});
    this.dbService.emitCountry();
    this.userAuthSubsc = this.dbService.userAuthSub.subscribe(
      (dataAuth)=>{
        if(dataAuth != null){
          this.init();
          setTimeout(() => {
            event.target.complete();
          }, 3000);
        }
    });
    this.dbService.emitUserAuth();
  }

  viewBack() { this.viewer = false;}

  routePage(index){
    this.router.navigate(['menu',index]);
  }

  ngOnDestroy() {
    this.countrySubsc.unsubscribe();
    this.userAuthSubsc.unsubscribe();
  }
}
